<?php

namespace App\Controller;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\Request\ParamFetcherInterface;

class ProductController extends AbstractFOSRestController
{
    /**
     * # Endpoint to filter json content by name & discount_percentage
     *
     * @FOSRest\RequestParam(name="name", requirements="[a-zA-Z0-9_ ]+", description="name string to filter by", strict=true, allowBlank=false, nullable=false)
     * @FOSRest\RequestParam(name="discount_percentage", requirements="\d+", description="discount_percentage number to filter by", strict=true, allowBlank=false, nullable=false)
     * @FOSRest\RequestParam(name="content", requirements=".+", description="Contents of JSON file", strict=true, allowBlank=false, nullable=false)
     *
     * @FOSRest\Route(path="/api/filter-products.{_format}", methods={"POST"}, requirements={"_format"="json|xml"})
     *
     * @FOSRest\View()
     */
    public function filterProducts(ParamFetcherInterface $paramFetcher): array
    {
        $params = $paramFetcher->all();

        $results = [];
        $products = json_decode($params['content'], true);

        foreach ($products as $product) {
            if (false !== strpos(strtolower($product['name']), strtolower($params['name']))
                && $params['discount_percentage'] === $product['discount_percentage']) {
                $results[] = $product;
            }
        }

        return $results;
    }
}
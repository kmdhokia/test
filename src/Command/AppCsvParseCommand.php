<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;

class AppCsvParseCommand extends Command implements ContainerAwareInterface
{
    protected static $defaultName = 'app:csv:parse';

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @return void
     */
    protected function configure(): void
    {
        $this
            ->addArgument('file_path', InputArgument::REQUIRED, 'A file_path of csv file.')
            ->setHelp('Pass a csv file_path to convert into JSON & XML.')
        ;
    }

    /**
     * @param ContainerInterface|null $container
     *
     * @return void
     */
    public function setContainer(ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $file = fopen($input->getArgument('file_path'), 'r');
            $results = [];
            $headers = fgetcsv($file, 1000, ',');

            while (($data = fgetcsv($file, 1000, ',')) !== FALSE) {
                $results[] = array_combine($headers, $data);
            }
            fclose($file);

            $this->saveContentToJsonFile($results);
        } catch (\Exception $ex) {
            $output->writeln($ex->getMessage());
        }

        $output->writeln('CSV file parse to JSON successfully!');

        return Command::SUCCESS;
    }

    /**
     * @param array $results
     *
     * @return void
     */
    private function saveContentToJsonFile(array $results): void
    {
        $uploadDir = $this->container->get('kernel')->getProjectDir().'/public/uploads/';
        $jsonFilename = sprintf('%s%s.%s', rand(1, 999), uniqid(), 'json');

        $filesystem = new Filesystem();
        $filesystem->dumpFile($uploadDir.$jsonFilename, json_encode($results, JSON_FORCE_OBJECT));
    }
}

Test App
========================
# Install dependencies 
`composer install`

# Start the symfony server
Once you have installed Symfony CLI, start the server using following command.

`symfony server:start`

Once server is ready, You'll be able to access it using `http://127.0.0.1:8000`

# Command to parse CSV file to JSON
Following command will be used to parse CSV file

`php bin/console app:csv:parse /path-to-csv-file/example.csv`

# Use the endpoint to filter by name and discount_percentage
To get response in JSON format please provide proper extension to endpoint
  `http://127.0.0.1:8000/api/filter-products.json`

OR

  `http://127.0.0.1:8000/api/filter-products.xml`

Based on the extension, You will receive response in either `json` or `xml` format

# Assumptions
- For endpoint, I've assumed that user will provide json `content`, string `name` and int `discount_percentage` 
all 3 parameters as I have kept them required.

# Things remaining in test
- I couldn't finish xml file generation in command
